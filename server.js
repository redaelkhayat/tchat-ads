var express = require("express");
var exphbs = require('express-handlebars');
var session = require('express-session');
var bodyParser = require('body-parser');
var app = express();
var http = require('http').Server(app);
var io = require("socket.io")(http);
var path = require("path");
var axios = require("axios");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true
}));

var hbs = exphbs.create({
    extname: ".hbs",
});

app.engine('.hbs', hbs.engine);
app.set('view engine', '.hbs');
app.set('views', path.resolve(__dirname, "views"));

var config = require("./config");
console.log(config);
var callApi = function (method, uri, headers, data) {
    headers = headers || {};
    data = data || {};
    console.log(config.url + uri);
    return axios({
        method: method,
        url: config.url + uri,
        data: data,
        headers: headers
    });
};
var tokenize = function (token, headers) {
    headers = headers || {};
    return Object.assign(headers, {
        "Authorization": "Bearer " + token
    });
}

app.post("/chat", function (req, res, next) {

    var login = function () {
        return callApi("post", "/connect/", null, req.body).then(function (response) {
            var token = response.data.access_token;
            req.session.token = token;
            return callApi("get", "/get-registration/", tokenize(token));
        }).then(function (response) {
            req.session.userId = response.data.data.user;
            return Promise.resolve();
        });
    };

    login().then(function () {
        return callApi("get", "/liste-registrations/", tokenize(req.session.token));
    }).then(function (response) {
        res.render("index", {
            users: response.data.data,
            id: req.session.userId,
            token: req.session.token
        });
    }).catch(function (err) {
        res.status(500).send({
            err: err
        });
    });

});

app.get("/login", function (req, res, next) {
    res.render("login");
});

io.on("connection", function (socket) {
    var roomId = null;

    /** keep userId in socket */
    socket.on("register", function (userId) {
        socket.userId = userId;
        console.log("user registred");
    });

    /** join|create conversation */
    socket.on("join", function (data) {

        console.log("join called");
        console.log(data);

        callApi("post", "/get-chat/", tokenize(data.token), {
            recepteur: data.receiver
        }).then(function (response) {
            var record = response.data.data;
            var chat = {
                token: data.token,
                id: record.id,
                room: "chat-" + record.id,
                transmitter: data.transmitter,
                receiver: record.emeteur == data.transmitter ? record.recepteur : record.emeteur,
            };

            roomId = chat.room;

            socket.join(chat.room); // join the room
            socket.chat = chat; // save within socket's object

            // return callApi("get", "/get-messages/?conversation=" + chat.id, tokenize(data.token));
            return Promise.resolve();
        }).then(function (response) {
            console.log("success");
            /* var messages = response.data.data;
            if(messages){
                messages = messages.slice(-1)[0]
            } */
            socket.emit("join", socket.chat);
            socket.broadcast.to(roomId).emit("online");
        }).catch(function (err) {
            console.log(err.toString());
        });
    });

    /** send a message */
    socket.on("send", function (text) {
        var chat = socket.chat;
        if (!chat) {
            return true;
        }
        callApi("post", "/get-messages/", tokenize(chat.token), {
            conversation_id: chat.id,
            msg: text,
            author_id: chat.transmitter
        }).then(function (response) {
            var msg = response.data.data;
            socket.emit("send", msg);

            var clientsInRoom = Object.keys(io.sockets.adapter.rooms[roomId].sockets);

            console.log(clientsInRoom);
            console.log("count clients in the room `" + roomId + "` " + clientsInRoom.length);

            if (clientsInRoom.length > 1) {
                socket.broadcast.to(roomId).emit("receive", msg);
            } else {
                var clients = io.sockets.connected;
                console.log('all clients: ');
                console.log(Object.keys(clients));
                console.log("receiver id: " + chat.receiver);
                for (var clientId in clients) {
                    var _socket = clients[clientId];
                    console.log(_socket.userId+ " - "+_socket.id);
                    if (chat.receiver == _socket.userId) {
                        console.log("message sent as offline mode to user `" + _socket.userId + "`");
                        _socket.emit("off receive", msg);
                    }
                }
            }

        }).catch(function (err) {
            console.log(err.toString());
        });
    });

    /** message status toggle */
    socket.on("seen", function (id) {
        console.log("`" + id + "` is seen")
        callApi("post", "/update-message/", tokenize(socket.chat.token), {
            msg_id: id
        }).then(function (response) {
            var message = response.data.data;
            if (message.status) {
                socket.broadcast.to(roomId).emit("seen", message);
            }
        }).catch(function (err) {
            console.log(err.toString());
        });
    });

    /** typing */
    socket.on("typing", function (status) {
        console.log("is typing");
        console.log(status);
        socket.broadcast.to(roomId).emit("typing", status);
    });

    /** listen to the `online` event */
    socket.on("online", function (status) {
        socket.broadcast.to(roomId).emit("online");
    });

    /** leave a room */
    socket.on("leave", function () {
        console.log("leaved");
        socket.broadcast.to(roomId).emit("offline");
        socket.leaveAll();
    });

    socket.on("disconnect", function () {
        socket.broadcast.to(roomId).emit("offline");
        console.log("a user is disconnected");
    });
});

http.listen(process.env.PORT || 3000, function () {
    console.log("listening...");
});
