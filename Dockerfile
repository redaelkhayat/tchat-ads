FROM node:carbon

WORKDIR /usr/src/app
ADD package*.json ./
RUN npm install
RUN npm i -g babel-cli babel-preset-es2015 babel-preset-stage-2
COPY . .
# ENV apiUrl=http://45440ae0.ngrok.io
# ENV PORT=3000

EXPOSE 8080
ENTRYPOINT ["npm", "start"]